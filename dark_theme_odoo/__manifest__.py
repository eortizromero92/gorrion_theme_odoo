# -*- coding: utf-8 -*-
{
	'name': 'Odoo Theme',
    'author': 'Edgar Ortiz',
    'version': '0.10',
	'depends': [],
    'price': 30.99,
    'currency': 'USD',
    'description':  '''
    Backend Theme for Odoo v.10.0
    ''',
    'images': [
        'images/account-theme-odoo.png',
        'images/invoice-odoo-theme.png',
        'images/login-theme-odoo.png',
        'images/odoo-theme-apps.png',
        'images/purchase-form-theme-odoo.png',
        'images/sales-form-theme-odoo.png',
        'images/sales-odoo-theme.png',
    ],
	'category': 'themes',
	'data': ['templates/data_theme.xml'],
	'installable': True
}
